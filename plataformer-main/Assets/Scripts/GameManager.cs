﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static bool gameOver = false;

    public static bool winCondition = false;

    public static int actualPlayer = 0;
    public static string nombreJugador;

    public List<Controller_Player> players;
    int limitejugadores;

    void Start()
    {
        gameOver = false;
        winCondition = false;
        actualPlayer = 0;
        limitejugadores = players.Count - 1;
    }

    void Update()
    {
        GetInput();
        nombreJugador = players[actualPlayer].nombre;// carga el nombre que despues es cargado en el codigo de Controle_Hub para mostrar el nombre del jugador actual
    }
    private void GetInput()// cambia de jugador dentro de los de la lista de jugadores
    {
        if (!winCondition)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                if (actualPlayer <= 0)
                {
                    actualPlayer = limitejugadores;
                }
                else
                {
                    actualPlayer--;
                }
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (actualPlayer >= limitejugadores)
                {
                    actualPlayer = 0;
                }
                else
                {
                    actualPlayer++;
                }
            }
        }
    }
}
