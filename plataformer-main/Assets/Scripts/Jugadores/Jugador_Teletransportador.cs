using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador_Teletransportador : Controller_Player
{
    public List<GameObject> jugadores;
    Vector3 poscicionAuciliar;
    private void Update()
    {
        if (GameManager.actualPlayer == indisejugador)// si el indise de jugador y el GameManager.actualPlayer coinsiden el personaje podra moverse
        {
            movimientoBase();
            teletransportar();
        }
        else
        {
            bloqueoBase();
        }
        activarEfecto();
    }
    void teletransportar()// camvia de posicion con otro jugador dentro de su lista
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space))
        {
            int j = Random.Range(0, jugadores.Count);
            poscicionAuciliar = transform.position;
            transform.position = jugadores[j].transform.position;
            jugadores[j].transform.position = poscicionAuciliar;
        }
    }
}
