using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador_Agrandamiento : Controller_Player
{
    bool grande = false;
    private void Update()
    {
        if (GameManager.actualPlayer == indisejugador)// si el indise de jugador y el GameManager.actualPlayer coinsiden el personaje podra moverse
        {
            movimientoBase();
            cresimiento();
        }
        else
        {
            bloqueoBase();
        }
        activarEfecto();
    }
    void cresimiento()// agranda la escala o recupera la escala inicial del jugador
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space))
        {
            if (grande)
            {
                transform.localScale = new Vector3(transform.localScale.x / 2, transform.localScale.y / 2, transform.localScale.z);
            }
            else
            {
                transform.localScale = new Vector3(transform.localScale.x * 2, transform.localScale.y * 2, transform.localScale.z);
            }
            grande = !grande;
        }
    }
}
