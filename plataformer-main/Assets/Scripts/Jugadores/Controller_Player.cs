﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    public int indisejugador;
    public static int multiplicador = 1;
    public float velocidad, inpulso;
    public Rigidbody rb;
    public float distanciaRayCast;
    public Material mat;
    public bool activo;
    public string nombre;
    private void Start()// carga de variables
    {
        rb = GetComponent<Rigidbody>();
        distanciaRayCast = transform.localScale.y / 2 + 0.1f;
        mat = GetComponent<Renderer>().material;
        activo = Convert.ToBoolean(mat.GetInt("_Activo"));
    }
    private void Update()
    {
        if (GameManager.actualPlayer == indisejugador && indisejugador == 0)// si el indise de jugador y el GameManager.actualPlayer coinsiden el personaje podra moverse
        {
            movimientoBase();
            saltoBase();
        }
        else// de lo contrario bloquea al personaje
        {
            bloqueoBase();
        }
        activarEfecto();
    }
    public void movimientoBase()// mueve al personaje de forma lateral
    {
        rb.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
        if (rb.velocity.x < 10 && rb.velocity.x > -10)
        {
            if (Input.GetKey(KeyCode.A))
            {
                rb.velocity -= Vector3.right * velocidad * Time.deltaTime;
            }
            else if (Input.GetKey(KeyCode.D))
            {
                rb.velocity += Vector3.right * velocidad * Time.deltaTime;
            }
        }

    }
    public void saltoBase()// permite el salto del personaje
    {
        if ((Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space)) && Physics.Raycast(transform.position, Vector3.down, distanciaRayCast))
        {
            rb.AddForce(Vector3.up * inpulso, ForceMode.Impulse);
        }
    }
    public void bloqueoBase()// bloquea el movimiento del personaje (meno el movimiento en eje y)
    {
        rb.velocity = new Vector3(0, rb.velocity.y, 0);
        rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
    }
    public void activarEfecto()// activa o desactiva el eveto visualdel shader que se usa para indicar el jugador actual
    {
        if (GameManager.actualPlayer == indisejugador)
        {
            activo = true;
        }
        else
        {
            activo = false;
        }
        mat.SetInt("_Activo", Convert.ToInt16(activo));
    }
}
