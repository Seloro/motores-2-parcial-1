using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador_Gravitacional : Controller_Player
{
    bool gravedadInvertida;
    private void Update()
    {
        if (GameManager.actualPlayer == indisejugador)// si el indise de jugador y el GameManager.actualPlayer coinsiden el personaje podra moverse
        {
            movimientoBase();
            camvioDeGravedad();
        }
        else
        {
            bloqueoBase();
        }
        gravedad();
        activarEfecto();
    }
    void camvioDeGravedad()// avilita el camvio de gravedad
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space))
        {
            gravedadInvertida = !gravedadInvertida;
        }
    }
    void gravedad()// si el cambio de gravedad esta avilitado, desavilita el uso de gravedad de unity del rigidbodi y aumenta su velocidad hacia arriva siempre que el ray cast no detecte piso
    {
        if (gravedadInvertida)
        {
            rb.useGravity = false;
            if (!Physics.Raycast(transform.position, Vector3.up, distanciaRayCast))
            {
                rb.velocity += Physics.gravity * -1 * Time.deltaTime;
            }
        }
        else
            rb.useGravity = true;
    }
}
