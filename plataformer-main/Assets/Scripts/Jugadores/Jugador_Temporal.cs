using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador_Temporal : Controller_Player
{
    float temp;
    public GameObject efectoTiempo;
    private void Update()
    {
        if (GameManager.actualPlayer == indisejugador)// si el indise de jugador y el GameManager.actualPlayer coinsiden el personaje podra moverse
        {
            movimientoBase();
            reducirTiempo();
        }
        else
        {
            bloqueoBase();
        }
        correguirTiempo();
        activarEfecto();
    }
    void reducirTiempo()// cambia la variable esatica multiplicador (la cual se usa en el movimiento de plataformas parareducir su velocidad
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space))
        {
            if (Time.timeScale == 1)
            {
                multiplicador = 4;
                efectoTiempo.SetActive(true);
            }
        }
    }
    void correguirTiempo()// despues de sierto tiempo restablese el valor de multiplicador a 1
    {
        if (efectoTiempo.activeSelf)
        {
            temp += Time.deltaTime;
            if (temp > 10)
            {
                temp = 0;
                multiplicador = 1;
                efectoTiempo.SetActive(false);
            }
        }
    }
}
