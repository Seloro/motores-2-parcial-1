using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jugador_Plataforma : Controller_Player
{
    bool modoPlataforma, puedoMoverme;
    float yBase;
    int signo = -1;
    public LayerMask piso, normal;
    public float limiteSuperior;
    private void Update()
    {
        if (GameManager.actualPlayer == indisejugador)// si el indise de jugador y el GameManager.actualPlayer coinsiden el personaje podra moverse
        {
            if (puedoMoverme)
            {
                movimientoBase();
            }
            activarPlataforma();
        }
        else
        {
            bloqueoBase();
        }
        plataforma();
        activarEfecto();
    }
    void activarPlataforma()// activa o desactiva el modoPlataforma. Si se desactiva cuandola posisionactual en y es menor a la posicion de y cuando se activo el modo, lo reposiciona a la posicion de activacion
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space) && Physics.Raycast(transform.position, Vector3.down, distanciaRayCast))
        {
            if (transform.position.y < yBase)
            {
                transform.position = new Vector3(transform.position.x, yBase, transform.position.z);
            }
            modoPlataforma = !modoPlataforma;
        }
    }
    void plataforma()// si el modo plataforma esta activo: al rigidbodi del jugador se le desactiva la gravedad de unity y al objeto se le permite ignorar coliciones conlos collider de layer piso, despues se mueve de arriba a abajo 
    {
        if (modoPlataforma)
        {
            rb.velocity = Vector3.zero;
            puedoMoverme = false;
            rb.useGravity = false;
            rb.excludeLayers = piso;
            transform.position += Vector3.up * inpulso * Time.deltaTime * multiplicador * signo;
            if (transform.position.y > yBase + limiteSuperior || transform.position.y < yBase - 2)
            {
                signo *= -1;
            }
        }
        else
        {
            puedoMoverme = true;
            rb.useGravity = true;
            rb.excludeLayers = normal;
            yBase = transform.position.y;
            signo = -1;
        }
    }
}
