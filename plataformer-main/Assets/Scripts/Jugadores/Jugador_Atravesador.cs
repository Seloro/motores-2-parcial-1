using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador_Atravesador : Controller_Player
{
    public LayerMask llave, bloqueo;
    bool cambio;
    private void Update()
    {
        if (GameManager.actualPlayer == indisejugador)// si el indise de jugador y el GameManager.actualPlayer coinsiden el personaje podra moverse
        {
            movimientoBase();
            cambiaColicion();
        }
        else
        {
            bloqueoBase();
        }
        activarEfecto();
    }
    void cambiaColicion()// permite atrabesar los collider que correspondar a los layers llave y bloqueo (solo puede atravesar uno al mismo tiempo)
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space))
        {
            cambio = !cambio;
        }
        if (cambio)
        {
            rb.excludeLayers = bloqueo;
        }
        else
        {
            rb.excludeLayers = llave;
        }
    }
}
