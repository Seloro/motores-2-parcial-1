using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador_Recolector : Controller_Player
{
    public int limiteDeLlaves;
    static public bool llaveActiva;
    int llavesActuales;
    private void Start()
    {
        llaveActiva = false;
        rb = GetComponent<Rigidbody>();
        mat = GetComponent<Renderer>().material;
        activo = Convert.ToBoolean(mat.GetInt("_Activo"));
    }
    private void Update()
    {
        if (GameManager.actualPlayer == indisejugador)// si el indise de jugador y el GameManager.actualPlayer coinsiden el personaje podra moverse
        {
            movimientoBase();
        }
        else
        {
            bloqueoBase();
        }
        if (llavesActuales == limiteDeLlaves)// si se recolectaron todas las llaves del nivel se establese verdadero el booleanostatico de llaveActiva que se usa en el codigo de elevador de llaves
        {
            llaveActiva = true;
        }
        activarEfecto();
    }
    private void OnCollisionEnter(Collision collision)// al colisionar con un objeto con etiqueta de llave lo destrulle y aumenta el contador de llaves
    {
        if (collision.gameObject.CompareTag("Llave"))
        {
            Destroy(collision.gameObject);
            llavesActuales++;
        }
    }
}
