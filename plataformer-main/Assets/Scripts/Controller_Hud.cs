﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public Text nombre, nivel, ganar;
    void Start()
    {
        ganar.gameObject.SetActive(false);
    }

    void Update()// muestra el nivel actual y el nombre del jugador
    {
        nombre.text = "Jugador: " + GameManager.nombreJugador;
        nivel.text = "Nivel: " + (Control_Nivel.nivel + 1);
        ganarCondicion();
    }
    void ganarCondicion()// si las condisiones de victoria se complen muestra el mensaje de ganar y presionar enter para pasr al siguiente nivel
    {
        if (GameManager.winCondition)
        {
            ganar.gameObject.SetActive(true);
        }
    }
}
