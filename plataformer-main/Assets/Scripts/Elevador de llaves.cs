using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevadordellaves : MonoBehaviour
{
    int signo = -1;
    public float limiteMax, limiteMin, velocidad;
    void Start()
    {
        limiteMax = transform.position.y + limiteMax;
        limiteMin = transform.position.y - limiteMin;
    }
    void Update()
    {
        movimiento();
    }
    void movimiento()// si se recolectaron las llaves mueve el elevadorde ariba a avajo
    {
        if (Jugador_Recolector.llaveActiva)
        {
            transform.position += Vector3.up * signo * Time.deltaTime * velocidad;
            if (transform.position.y > limiteMax || transform.position.y < limiteMin)
                signo *= -1;
        }
    }
}
