using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meta : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)// si el jugador entra en contacto con la meta vuelve verdadera la condicion de victoria y establese en 0 la escala de tiempo
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager.winCondition = true;
            Time.timeScale = 0;
        }
    }
}
