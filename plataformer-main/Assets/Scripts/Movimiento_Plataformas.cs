using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento_Plataformas : MonoBehaviour
{
    public bool vertical, horizontal;
    public float limiteMaxV, limiteMinV, limiteMaxH, limiteMinH, velocidad;
    public int signoV, signoH;
    void Start()// establese bariables de limites de distancia vertical y/o horizontal y sus signos 8de no estar establesidos)
    {
        limiteMaxH = transform.position.x + limiteMaxH;
        limiteMinH = transform.position.x - limiteMinH;
        limiteMaxV = transform.position.y + limiteMaxV;
        limiteMinV = transform.position.y - limiteMinV;
        if (signoH == 0)
            signoH = 1;
        if (signoV == 0)
            signoV = 1;
    }
    void Update()
    {
        movimientoHorizontal();
        movimientoVertical();
    }
    void movimientoVertical()// mueve la plataforma de forma vertical
    {
        if (vertical)
        {
            transform.position += (Vector3.up * signoV * Time.deltaTime * velocidad) / Controller_Player.multiplicador;
            if (transform.position.y < limiteMinV || transform.position.y > limiteMaxV)
                signoV *= -1;
        }
    }
    void movimientoHorizontal()// mueve la plataforma de forma horizontal
    {
        if (horizontal)
        {
            transform.position += (Vector3.right * signoH * Time.deltaTime * velocidad) / Controller_Player.multiplicador;
            if (transform.position.x < limiteMinH || transform.position.x > limiteMaxH)
                signoH *= -1;
        }
    }
    /* en caso de que se quiera se podria hacer que la plataforma se mueve tanto en el eje vertical como horizontal
     (no se realiza la combinacion en este proyecto)*/ 
}
